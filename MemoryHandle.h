#pragma once

#include "HashTable.h"
#include "Handle.h"

#include "Types.h"

#include <new>

typedef THashTable< CHandle*, const CHandle*, u64 > CHandleTable;

class CMemoryHandleTable
{
public:
	static CMemoryHandleTable*	GetActiveTable()	{ return ms_pActiveTable; }

public:
							CMemoryHandleTable();
							~CMemoryHandleTable();
	template< typename DataType >
	THandle< DataType >*	GetMemoryHandle( DataType* pMemory );
	void					ReleaseMemoryHandle( CHandle& rHandle );
	void					OnFree( void* pMemory );

private:
	static CMemoryHandleTable*	ms_pActiveTable;

private:
	CHandleTable		m_oHandleTable;
	CMemoryHandleTable*	m_pParent;
};
