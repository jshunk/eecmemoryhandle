#include "MemoryHandle.h"
#include "Macros.h"

#include "Handle.inl"

template< typename DataType >
THandle< DataType >* CMemoryHandleTable::GetMemoryHandle( DataType* pMemory )
{
	assert( pMemory );
	THandle< DataType >* pResult( new THandle< DataType >( pMemory ) );
	m_oHandleTable.Store( reinterpret_cast< ptr >( pMemory ), pResult );
	return pResult;
}
