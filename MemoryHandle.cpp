#include "MemoryHandle.h"

#include "Macros.h"

#include <new>

#include "HashTable.inl"

CMemoryHandleTable* CMemoryHandleTable::ms_pActiveTable( nullptr );

CMemoryHandleTable::CMemoryHandleTable()
	: m_pParent( GetActiveTable() )
	, m_oHandleTable( 100, nullptr )
{
	ms_pActiveTable = this;
}

CMemoryHandleTable::~CMemoryHandleTable()
{
	assert( m_oHandleTable.IsEmpty() );
	ms_pActiveTable = m_pParent;
}

void CMemoryHandleTable::ReleaseMemoryHandle( CHandle& rHandle )
{
	m_oHandleTable.RemovePair( rHandle.GetKey(), &rHandle );
	delete &rHandle;
}

void CMemoryHandleTable::OnFree( void* pMemory )
{
	// Release all handles:
	ptr uKey( reinterpret_cast< ptr >( pMemory ) );
	CHandle* pHandle;
	u16 uNumHandles( m_oHandleTable.Retrieve( uKey, &pHandle, 1 ) );
	for( u16 i( 0 ); i < uNumHandles; ++i )
	{
		assert( pHandle );
		pHandle->Disable();
		m_oHandleTable.RemovePair( uKey, pHandle );
		if( i < uNumHandles - 1 )
		{
			m_oHandleTable.Retrieve( uKey, &pHandle, 1 );
		}
	}
}

void operator delete( void * pMemory ) throw()
{
	CMemoryHandleTable* pActiveTable( CMemoryHandleTable::GetActiveTable() );
	if( pActiveTable )
	{
		pActiveTable->OnFree(pMemory);
	}
	free( pMemory );
}
